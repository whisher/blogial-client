export interface ConfigUrls {
  authentication: {
    account: string;
    login: string;
  },
  posts: string;
  users: string;
  logins: string;
  pwa: {
    subscription: string;
    notification: string;
  }
}
