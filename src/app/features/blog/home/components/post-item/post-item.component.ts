import { Component, OnInit, Input, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';

import { Post, Image } from '../../../../../core/posts/models';


@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  selector: 'blog-home-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.scss'],
})
export class BlogHomePostItemComponent implements OnInit{
  @Input() post: Post;
  places: string;
  tags: [string];
  images: Image[];

  ngOnInit(){
    this.places = JSON.parse(this.post.places)
    .map(place => place.name)
    .join(' ,');
    this.tags = JSON.parse(this.post.tags);
    this.images = JSON.parse(this.post.images);
  }

}
