import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxMdModule } from 'ngx-md';

// Modules
import { BlogHomeRoutingModule } from './home-routing.module';
import { IconsModule } from '../../../shared/icons/icons.module';
import { UiButtonsModule } from '../../../shared/ui/buttons/buttons.module';
import { UiLoaderModule } from '../../../shared/ui/loader/loader.module';
import { UiReadMoreModule } from '../../../shared/ui/read-more/read-more.module';

import { UiUtilitiesModule } from '../../../shared/ui/utilities/utilities.module';

import * as fromComponents from './components';
import * as fromContainers from './containers';

@NgModule({
  imports: [
    CommonModule,
    NgxMdModule,
    BlogHomeRoutingModule,
    IconsModule,
    UiButtonsModule,
    UiButtonsModule,
    UiLoaderModule,
    UiReadMoreModule,
    UiUtilitiesModule
  ],
  declarations: [
    ...fromComponents.components,
    ...fromContainers.containers
  ]
})
export class BlogHomeModule { }
