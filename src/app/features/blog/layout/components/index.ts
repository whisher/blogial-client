import { BlogLayoutBrandComponent } from './brand/brand.component';
import { BlogLayoutNavComponent } from './nav/nav.component';

export const components: any[] = [
  BlogLayoutBrandComponent,
  BlogLayoutNavComponent
];
