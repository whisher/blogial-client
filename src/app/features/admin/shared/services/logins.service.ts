import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { URLS } from '../../../../config/config.tokens';
import { HttpErrorHandler } from '../../../../shared/http/http-error-handler';
import { Login } from '../models';

@Injectable()
export class LoginsService {
  loginsUrl: string;
  constructor(
    private http: HttpClient,
    @Inject(URLS) private urls
  ) {
    this.loginsUrl = urls.logins;
  }

  findByUserId(): Observable<Login[]> {
    return this.http.get<Login[]>(`${this.loginsUrl}`)
    .pipe(catchError((error: any) => HttpErrorHandler.handle(error)));
  }
}
