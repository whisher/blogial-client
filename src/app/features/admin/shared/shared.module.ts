import { NgModule, ModuleWithProviders } from '@angular/core';

import * as fromService from './services';

@NgModule({
  imports: []
})
export class AdminSharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AdminSharedModule,
      providers: [
        ...fromService.services
      ]
    };
  }
}
