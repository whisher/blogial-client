import { NgModule } from '@angular/core';

import { AdminLayoutModule } from './layout/layout.module';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminSharedModule } from './shared/shared.module';
@NgModule({
  imports: [
    AdminLayoutModule,
    AdminRoutingModule,
    AdminSharedModule.forRoot()
  ]
})
export class AdminModule { }
