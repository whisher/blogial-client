import { AdminDashboardLoginsComponent } from './logins/logins.component';
import { AdminDashboardWelcomeComponent } from './welcome/welcome.component';

export const components: any[] = [
  AdminDashboardLoginsComponent,
  AdminDashboardWelcomeComponent
];
