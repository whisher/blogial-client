import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDashboardLoginsComponent } from './logins.component';

describe('AdminDashboardLoginsComponent', () => {
  let component: AdminDashboardLoginsComponent;
  let fixture: ComponentFixture<AdminDashboardLoginsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDashboardLoginsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDashboardLoginsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
