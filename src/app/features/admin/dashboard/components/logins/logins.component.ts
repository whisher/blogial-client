import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { Login } from '../../../shared/models';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-dashboard-logins',
  template: `
    <h3>Last logins:</h3>
    <ul class="list-group list-group-flush">
      <li class="list-group-item" *ngFor="let login of logins">
       {{login.time | date:'medium'}}
      </li>
    </ul>
  `,
  styles: [`
    :host{
      display: block;
    }
  `],
  host:{
    class: 'my-3'
  }
})
export class AdminDashboardLoginsComponent{
  @Input() logins: Login[];
  constructor() { }
}
