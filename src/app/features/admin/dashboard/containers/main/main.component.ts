import { Component, OnInit, ChangeDetectionStrategy} from '@angular/core';
import { Store, select } from '@ngrx/store';

import { Observable,of } from 'rxjs';
import { map } from 'rxjs/operators';

import * as fromAuthentication from '../../../../../core/authentication/store';
import { Login } from '../../../shared/models';
import { LoginsService } from '../../../shared/services';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'admin-dashboard-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class AdminDashboardMainComponent implements OnInit{
  account$ = this.store.select(fromAuthentication.getAccount);
  logins$: Observable<Login[]>;
  constructor(
    private store: Store<fromAuthentication.State>,
    private loginsService: LoginsService
  ) {}

  ngOnInit(){
    this.logins$ = this.loginsService.findByUserId()
    .pipe(map(logins => logins.slice(0, 5)));
  }
}
