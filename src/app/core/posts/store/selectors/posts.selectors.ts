import { createSelector } from '@ngrx/store';

import * as fromRoot from '../../../../store';
import * as fromFeature from '../reducers';
import * as fromPosts from '../reducers/posts.reducer';
import { Post } from '../../models';

export const featurePostsState = createSelector(
  fromFeature.getPostsState,
  (state: fromFeature.State) => state.posts
);

export const getPostsEntities = createSelector(
  featurePostsState,
  fromPosts.getPostsEntities
);

export const getSelectedPostById = createSelector(
  featurePostsState,
  fromRoot.getRouterState,
  (entities, router): Post => {
    return router.state && entities[router.state.params.id];
  }
);

export const getSelectedPostBySlug = createSelector(
  getPostsEntities,
  fromRoot.getRouterState,
  (entities, router) => {
    const values = Object.values(entities) as Post[];
    return values.filter(post => post.slug===router.state.params.slug)[0];
  }
);

export const getAllPosts = createSelector(
  getPostsEntities,
  entities => {
  return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
});

export const getHasPosts = createSelector(
  getPostsEntities,
  entities => {
  return !!Object.keys(entities).length;
});

export const getPostsLoaded = createSelector(
  featurePostsState,
  fromPosts.getPostsLoaded
);

export const getPostLoaded = createSelector(
  featurePostsState,
  fromPosts.getPostLoaded
);

export const getPostsLoading = createSelector(
  featurePostsState,
  fromPosts.getPostsLoading
);

export const getPostsError = createSelector(
  featurePostsState,
  fromPosts.getPostsError
);
