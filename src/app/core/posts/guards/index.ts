import { PostGuard } from './post.guard';
import { PostsGuard } from './posts.guard';

export const guards: any[] = [
  PostGuard,
  PostsGuard
];

export * from './post.guard';
export * from './posts.guard';
