import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiReadMoreComponent } from './read-more.component';

describe('UiReadMoreComponent', () => {
  let component: UiReadMoreComponent;
  let fixture: ComponentFixture<UiReadMoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiReadMoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiReadMoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
