import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiReadMoreComponent } from './read-more.component';

const UI_READ_MORE = [UiReadMoreComponent];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: UI_READ_MORE,
  exports: UI_READ_MORE
})
export class UiReadMoreModule { }
