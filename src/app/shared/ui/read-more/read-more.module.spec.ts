import { UiReadMoreModule } from './read-more.module';

describe('UiReadMoreModule', () => {
  let readMoreModule: UiReadMoreModule;

  beforeEach(() => {
    readMoreModule = new UiReadMoreModule();
  });

  it('should create an instance', () => {
    expect(readMoreModule).toBeTruthy();
  });
});
