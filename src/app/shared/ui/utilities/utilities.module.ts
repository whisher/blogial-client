import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UiGoToTopDirective } from './directives/go-to-top.directive';
import { UiHeaderScrollDirective } from './directives/header-scroll.directive';
import { UiImageLoaderDirective } from './directives/image-loader.directive';
import { UiRoleDirective } from './directives/role.directive';
import { UiSubscribeDirective } from './directives/subscribe.directive';
import { WINDOW_PROVIDERS } from './services/window.service';

const UI_UTILITIES = [
  UiGoToTopDirective,
  UiHeaderScrollDirective,
  UiImageLoaderDirective,
  UiRoleDirective,
  UiSubscribeDirective
];

@NgModule({
  imports: [CommonModule],
  declarations: UI_UTILITIES,
  providers: [ WINDOW_PROVIDERS ],
  exports: UI_UTILITIES
})
export class UiUtilitiesModule { }
