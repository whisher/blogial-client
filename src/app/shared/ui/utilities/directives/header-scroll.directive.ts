import {Directive, Inject, HostBinding, HostListener} from '@angular/core';

import { WINDOW } from '../services/window.service';

@Directive({
  selector: '[uiHeaderScroll]',
})
export class UiHeaderScrollDirective {
    @HostBinding('class') private cls = 'isBgHeaderDefault o-container-fixed';
    @HostListener('window:scroll', ['$event'])
    onScroll(){
      const windowScroll = this.window.pageYOffset;
      const isScrolled = windowScroll > 0;
      this.cls = isScrolled ? 'isBgHeaderScrolled o-container-fixed' : 'isBgHeaderDefault o-container-fixed';
    }
    constructor(
      @Inject(WINDOW) private window
    ) { }
}
