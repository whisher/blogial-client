import {
  Directive,
  Attribute,
  Renderer2,
  ElementRef,
  HostListener } from '@angular/core';

@Directive({
  selector: '[uiImageLoader]'
})
export class UiImageLoaderDirective {
  loaderSrc = '/assets/images/image-loader.svg';
  onErrorSrc = '/assets/images/no-available-800.png';
  constructor(
    private renderer: Renderer2,
    private el: ElementRef) {
      this.renderer.setAttribute(this.el.nativeElement, 'src', this.loaderSrc);
    }

  @HostListener('load') onLoad() {
    this.renderer.setAttribute(this.el.nativeElement, 'src', this.el.nativeElement.src);
  }
  @HostListener('error') onError() {
    this.renderer.setAttribute(this.el.nativeElement, 'src', this.onErrorSrc);
  }
}
